![Mozilla Add-on rating](https://img.shields.io/amo/stars/view-source-switch?style=flat-square) ![Mozilla Add-on version](https://img.shields.io/amo/v/view-source-switch?style=flat-square)  

<img src="images/icon.png" width="150"/>

<h1>View Source Switch</h1>

View Source Switch is a Firefox add-on that allows one click switching the active tab between "view source" and regular web page.  

[![Get the add-on !](https://extensionworkshop.com/assets/img/documentation/publish/get-the-addon-178x60px.dad84b42.png)](https://addons.mozilla.org/firefox/addon/view-source-switch/)

<h2>How does it work ?</h2>

Once the add-on is installed, a new icon appears in Firefox toolbar :  

![Firefox toolbar](images/ViewSourceSwitch_browser_action.png)

Click on it to switch to the view-source of your active tab. Click again to switch back to the regular view :  

![View Source Switch demonstration](images/ViewSourceSwitch_demo.png)

Instead of clicking, you can switch page with a keyboard shortcut. The default one is <strong>Ctrl + Shift + U</strong> / <strong>command + shift + U</strong>.  
This key combination is intended to be similar to the Firefox built-in shortcut to view source in a new tab.  
However, this key combination can easily be changed in the Firefox <a href="about:addons">add-on menu</a> :  

![Update extensions keyboard shortcuts](images/Manage_extension_shortcuts.png)

You can change the color scheme used for syntax highlight as well as the line wrapping in the extension preferences :  
![Update extensions keyboard shortcuts](images/ViewSourceSwitch_preferences.png)

<h3>What permissions are needed ?</h3>

View Source Switch requires the following permission :

<strong>Access your data for all websites</strong>  

Per <a href="https://support.mozilla.org/en-US/kb/permission-request-messages-firefox-extensions#w_access-your-data-for-all-websites">Mozilla documentation</a>, 
"Access your data for all websites" permission means that "The extension can read the content of any web page you 
visit as well as data you enter into those web pages, such as usernames and passwords".  
This permission is needed to get the source code of the page, apply highlighting on it, 
and then re-render this highlighted source code.

<strong>Access browser tabs</strong>  

Per <a href="https://support.mozilla.org/en-US/kb/permission-request-messages-firefox-extensions#w_access-browser-tabs">Mozilla documentation</a>, 
"Access browser tabs" permission means that "The extension could obtain the URL, title, 
and icon from any tab. ".  
This permission is needed to maintain the View Source Switch icon state according to 
the currently displayed URL.

<h3>Does View Source Switch collect my data ?</h3>

No.  

<h3>Does View Source Switch send my data to anyone ?</h3>

No. 

<h2>Build from source</h2>  

Prerequisite :  
Install <a href="https://nodejs.dev/learn/how-to-install-nodejs">Node.js and npm</a>.  

Build :  
The add-on can be built by running the following command from its root directory :  
```
npm ci  
npm run build  
```

Highlight.JS :  
Syntax highlighting and theming is made thanks to <a href="https://highlightjs.org/">Highlight.js</a> 
library. View Source Switch includes a build of the library for XML, Javascript and CSS.  
This can be done on "*nix" environment with the following commands :  
```bash
git clone https://github.com/highlightjs/highlight.js.git
cd highlight.js
npm install
node tools/build.js xml javascript css
cd ..
mkdir highlightjs highlightjs/styles
cp -r highlight.js/build/demo/styles/* highlightjs/styles
cp highlight.js/build/highlight.min.js highlightjs
cp highlight.js/LICENSE highlightjs
rm -r highlight.js
```
On Windows, this should be adapted.  
Further information on building the library can be found here :  
https://highlightjs.readthedocs.io/en/latest/building-testing.html#building

<h2>Legal notice</h2>

Firefox is a registered trademark of the Mozilla Foundation.  
Windows is a registered trademark of Microsoft Corporation.  
Node.js is a trademark of the OpenJS Foundation.  

The above-mentioned trademarks are only used to refer to products.  
View Source Switch and its developer are not affiliated, sponsored nor endorsed by any of the above-mentioned organizations.  

<h2>Changelog</h2>  

4.0.0 -> update Highlight.js, web-ext and Typescript versions, add possibility to choose source code or rendered code, add GitLab CI config, light/dark icons  
3.1.0 -> update web.ext version, fix long lines wrap issue  
3.0.1 -> fix bux in option page, initialize all extension local storage values in background  
3.0.0 -> new context menu to change word wrap, add 166 new themes, add line numbers, better take line breaks into account, add choice between native source viewer and VSS one, update homepage and repo URLs, re-structured code  
2.0.0 -> new option to change highlight colors  
1.0.0 -> first version  
0.0.1 -> first commit  
