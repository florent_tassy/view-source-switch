/************************************************************************************/
// Global variables definition
/************************************************************************************/

//@ts-ignore
let browser = browser;

enum preferredCodeEnum {
    //@ts-ignore
    rendered = "rendered",
    //@ts-ignore
    source = "source"
}

let currentPageSource: string = "";

/************************************************************************************/
// Functions definition
/************************************************************************************/

// Function that copy the current page source 
// into the currentPageSource global variable.
async function setPageSource() {
    currentPageSource = "";
    const getPreferredCode = await browser.storage.local.get("vssCode");
    let preferredCode = await getPreferredCode["vssCode"];
    switch (preferredCode) {
        case preferredCodeEnum.source:
            try {
                const response = await fetch(window.location.href);
                currentPageSource = await response.text();
            } catch (err) {
                console.error(err);
                currentPageSource = new XMLSerializer().serializeToString(document);
            }
            break;
        case preferredCodeEnum.rendered:
        default:
            currentPageSource = new XMLSerializer().serializeToString(document);
    }
}

/************************************************************************************/
// Listeners definition
/************************************************************************************/

browser.runtime.onMessage.addListener(async (request: any) => {
    await setPageSource();
    return Promise.resolve({ response: currentPageSource });
});
