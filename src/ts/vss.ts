/************************************************************************************/
// Global variables definition
/************************************************************************************/

let vssUrl: string = window.location.search;

let searchParams: any = new URLSearchParams(vssUrl);
let urlToDisplay: string = searchParams.get("url") ? searchParams.get("url") : "vss.html";

let srcToDisplay: string = "";


/************************************************************************************/
// Functions definition
/************************************************************************************/

// Function that sends a message to background.
async function requestSourceCodeToBackground(e: any) {
    try {
        //@ts-ignore
        const message = await browser.runtime.sendMessage({
            request: "From vss.js: source code needed."
        });
        handleResponse(message);
    } catch (error) {
        handleError(error);
    }
}

// Function called when backgrounds sends back 
// string reprensation of source code.
// Sets the root element innerText. 
function handleResponse(message: any) {
    srcToDisplay = message.response;
    let vssRootElement: HTMLElement | null = document.getElementById("vssRootElement");

    if (vssRootElement === null) {
        return;
    }

    vssRootElement.textContent = srcToDisplay;

    document.title = urlToDisplay;

    //@ts-ignore
    hljs.highlightElement(vssRootElement);

    //@ts-ignore
    hljs.initLineNumbersOnLoad();
}

// Function called when backgrounds scannot sends  
// back the string reprensation of source code.
function handleError(error: any) {
    console.warn("View Source Switcher: Error while requesting source code to background...\n" + error);
}

/************************************************************************************/
// Listeners definition
/************************************************************************************/

// Request source code to display
window.addEventListener("load", requestSourceCodeToBackground);
