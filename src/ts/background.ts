/************************************************************************************/
// Global variables definition
/************************************************************************************/

// @ts-ignore
let browser = browser;

enum preferredCodeEnum {
    //@ts-ignore
    rendered = "rendered",
    //@ts-ignore
    source = "source"
}

const vssCodeKey = "vssCode";
const vssWordWrapKey = "vssWordWrap";
const vssStyleKey = "vssStyle";
const vssNativeViewerKey = "vssNativeViewer";

let currentUrl: string = '';
let currentId: number = 0;
let currentSource: string;

let preferredWordWrap: boolean = false;

const prefix: string = "view-source:";

/************************************************************************************/
// Functions definition
/************************************************************************************/

// Function tied to the browser action button.
// Retrieve URL and tab id of the active tab
// Add or remove a "view-source:" prefix to
// this URL.
async function switchUrl() {

    let currentTab: any;
    let newUrl: string = "";
    let goToVSS: boolean = false;

    let preferredNativeViewer: boolean = false;
    const getPreferredNativeViewer: any = await browser.storage.local.get(vssNativeViewerKey);
    preferredNativeViewer = getPreferredNativeViewer[vssNativeViewerKey];

    try {
        let tabs: any = await browser.tabs.query({ currentWindow: true, active: true });
        currentTab = tabs[0];
        currentUrl = currentTab.url;
        currentId = currentTab.id;

        // Tab is already in "view-source" mode -> Go back to regular view
        if (currentUrl.startsWith(prefix)) {
            newUrl = currentUrl.replace(prefix, '');
        }
        // Tab is already in "VSS" mode -> Go back to regular view
        else if (currentUrl.startsWith("moz-extension://") && currentUrl.indexOf("vss.html") > -1) {
            const parsedUrl: URL = new URL(currentUrl);
            const originUrl: string | null = parsedUrl.searchParams.get("url");
            originUrl ? newUrl = originUrl : newUrl = currentUrl;
        }
        // Will go to Firefox view-source mode
        else if (preferredNativeViewer) {
            newUrl = "view-source:" + currentUrl;
        }
        // Page will go to VSS mode
        else {
            newUrl = "vss.html?url=" + currentUrl;
            goToVSS = true;
        }
    } catch (err) {
        console.warn("View Source Switch: Could not retrieve the current tab...\n", err);
    }

    try {
        if (goToVSS) {
            await requestSourceCodeToTabs(currentId);
        }

        await browser.tabs.update(currentId, { active: true, url: newUrl });
    } catch (err) {
        console.warn("View Source Switch: Could not update the current tab to " + newUrl + "\n", err);
    } finally {
        setCurrentUrlAndIcon();
    }
}

// Function that update the currentUrl global
// variable and set the browser action button
// accordingly.
async function setCurrentUrlAndIcon() {
    try {
        let tabs = await browser.tabs.query({ currentWindow: true, active: true });
        let currentTab = await tabs[0];
        currentUrl = await currentTab.url;
        currentId = await currentTab.id;
    } catch (err) {
        console.warn("View Source Switch: Could not retrieve the current tab...\n", err);
    } finally {
        setBrowserIcon(currentUrl);
        addOrRemoveContextMenuItem();
    }
}

// Function that set browser action button
// icon depending on the URL in paramater.
function setBrowserIcon(url: string) {
    const iconTheme = window.matchMedia("(prefers-color-scheme: dark)").matches ? "light" : "dark";
    if (url.startsWith(prefix) || (url.startsWith("moz-extension://") && url.indexOf("vss.html") > -1)) {
        browser.browserAction.setIcon({ path: `icons/${iconTheme}-icon_on.svg` });
    } else {
        browser.browserAction.setIcon({ path: `icons/${iconTheme}-icon.svg` });
    }
}

// Function called back when a tab is updated
function handleUpdated(tabId: number, changeInfo: any, tabInfo: any) {
    if (changeInfo.url) {
        currentUrl = changeInfo.url;
        setBrowserIcon(currentUrl);
        addOrRemoveContextMenuItem();
    }
}

// Function that request the source code of a 
// tab given as parameter.
// Updates the value of global variable 
// currentSource.
async function requestSourceCodeToTabs(tabs: any): Promise<void> {
    try {
        const message = await browser.tabs.sendMessage(
            tabs,
            { request: "From background.js: source code needed." }
        )
        currentSource = message.response;
    } catch (error) {
        onRequestError(error);
    }
}

// Function called back if a tab did not send 
// back a response.
function onRequestError(error: any) {
    console.warn("View Source Switch error in 'requestSourceCodeToTabs' function :\n", error);
}

// Function called when background script  
// receives a message from a tab.
// Sends the content of currentSource to 
// the requesting tab.
function handleVSSMessage(request: any, sender: any, sendResponse: any) {
    console.info("Received from content script :", request);
    sendResponse({ response: currentSource });
}

// Function that switches word wrap option
// Function that writes the word wrap value 
// into local storage
async function switchWordWrap() {
    const getPreferredWordWrap: any = await browser.storage.local.get(vssWordWrapKey);
    preferredWordWrap = getPreferredWordWrap[vssWordWrapKey];

    if (!preferredWordWrap) {
        preferredWordWrap = false;
    }

    preferredWordWrap = !preferredWordWrap;

    browser.storage.local.set({ "vssWordWrap": preferredWordWrap });
}


/************************************************************************************/
// Initialize local storage default values
/************************************************************************************/
async function initializeLocalStorage() {
    let getPreferredViewer = await browser.storage.local.get(vssNativeViewerKey);
    let preferredViewer = await getPreferredViewer[vssNativeViewerKey];

    if (preferredViewer === undefined) {
        browser.storage.local.set({ "vssNativeViewer": false });
    }

    let getPreferredCode = await browser.storage.local.get(vssCodeKey);
    let preferredCode = await getPreferredCode[vssCodeKey];

    if (preferredCode === undefined) {
        browser.storage.local.set({ "vssCode": preferredCodeEnum.rendered });
    }

    const getPreferredWordWrap: any = await browser.storage.local.get(vssWordWrapKey);
    let preferredWordWrap: boolean = await getPreferredWordWrap[vssWordWrapKey];

    if (preferredWordWrap === undefined) {
        browser.storage.local.set({ "vssWordWrap": false });
    }

    let getPreferredStyle = await browser.storage.local.get(vssStyleKey);
    let preferredStyle = await getPreferredStyle[vssStyleKey];

    if (preferredStyle === undefined) {
        browser.storage.local.set({ "vssStyle": "default" });
    }

}

initializeLocalStorage();

/************************************************************************************/
// Listeners definition
/************************************************************************************/

browser.tabs.onActivated.addListener(setCurrentUrlAndIcon);
browser.tabs.onUpdated.addListener(handleUpdated);
browser.browserAction.onClicked.addListener(switchUrl);
browser.runtime.onMessage.addListener(handleVSSMessage);

/************************************************************************************/
// Context menu
/************************************************************************************/

function createContextMenu(): void {
    browser.contextMenus.create({
        id: "contextMenuEntry",
        title: "Wrap long lines",
        type: "checkbox",
        checked: preferredWordWrap,
        contexts: ["page"],
        onclick: () => { switchWordWrap(); }
    });
}

async function addOrRemoveContextMenuItem() {
    const getPreferredWordWrap: any = await browser.storage.local.get(vssWordWrapKey);
    preferredWordWrap = getPreferredWordWrap[vssWordWrapKey];
    if (currentUrl.startsWith("moz-extension://") && currentUrl.indexOf("vss.html") > -1) {
        createContextMenu();
    } else {
        browser.contextMenus.removeAll();
    }
}
