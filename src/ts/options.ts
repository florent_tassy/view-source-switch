/************************************************************************************/
// Global variables definition
/************************************************************************************/

enum preferredCodeEnum {
    //@ts-ignore
    rendered = "rendered",
    //@ts-ignore
    source = "source"
}

const styles: string[] = [
    "3024",
    "a11y-dark",
    "a11y-light",
    "agate",
    "androidstudio",
    "an-old-hope",
    "apathy",
    "apprentice",
    "arduino-light",
    "arta",
    "ascetic",
    "ashes",
    "atelier-cave",
    "atelier-cave-dark",
    "atelier-cave-light",
    "atelier-dune",
    "atelier-dune-dark",
    "atelier-dune-light",
    "atelier-estuary",
    "atelier-estuary-dark",
    "atelier-estuary-light",
    "atelier-forest",
    "atelier-forest-dark",
    "atelier-forest-light",
    "atelier-heath",
    "atelier-heath-dark",
    "atelier-heath-light",
    "atelier-lakeside",
    "atelier-lakeside-dark",
    "atelier-lakeside-light",
    "atelier-plateau",
    "atelier-plateau-dark",
    "atelier-plateau-light",
    "atelier-savanna",
    "atelier-savanna-dark",
    "atelier-savanna-light",
    "atelier-seaside",
    "atelier-seaside-dark",
    "atelier-seaside-light",
    "atelier-sulphurpool",
    "atelier-sulphurpool-dark",
    "atelier-sulphurpool-light",
    "atlas",
    "atom-one-dark",
    "atom-one-dark-reasonable",
    "atom-one-light",
    "bespin",
    "black-metal-bathory",
    "black-metal-burzum",
    "black-metal",
    "black-metal-dark-funeral",
    "black-metal-gorgoroth",
    "black-metal-immortal",
    "black-metal-khold",
    "black-metal-marduk",
    "black-metal-mayhem",
    "black-metal-nile",
    "black-metal-venom",
    "brewer",
    "bright",
    "brogrammer",
    "brown-paper",
    "brush-trees",
    "brush-trees-dark",
    "chalk",
    "circus",
    "classic-dark",
    "classic-light",
    "codepen-embed",
    "codeschool",
    "color-brewer",
    "colors",
    "cupcake",
    "cupertino",
    "danqing",
    "darcula",
    "dark",
    "darkmoss",
    "darktooth",
    "dark-violet",
    "decaf",
    "default",
    "default-dark",
    "default-light",
    "devibeans",
    "dirtysea",
    "docco",
    "dracula",
    "edge-dark",
    "edge-light",
    "eighties",
    "embers",
    "equilibrium-dark",
    "equilibrium-gray-dark",
    "equilibrium-gray-light",
    "equilibrium-light",
    "espresso",
    "eva",
    "eva-dim",
    "far",
    "felipec",
    "flat",
    "foundation",
    "framer",
    "fruit-soda",
    "gigavolt",
    "github",
    "github-dark",
    "github-dark-dimmed",
    "github-gist",
    "gml",
    "googlecode",
    "google-dark",
    "google-light",
    "gradient-dark",
    "gradient-light",
    "grayscale",
    "grayscale-dark",
    "grayscale-light",
    "green-screen",
    "gruvbox-dark",
    "gruvbox-dark-hard",
    "gruvbox-dark-medium",
    "gruvbox-dark-pale",
    "gruvbox-dark-soft",
    "gruvbox-light",
    "gruvbox-light-hard",
    "gruvbox-light-medium",
    "gruvbox-light-soft",
    "hardcore",
    "harmonic16-dark",
    "harmonic16-light",
    "heetch-dark",
    "heetch-light",
    "helios",
    "hopscotch",
    "horizon-dark",
    "horizon-light",
    "humanoid-dark",
    "humanoid-light",
    "hybrid",
    "ia-dark",
    "ia-light",
    "icy-dark",
    "idea",
    "intellij-light",
    "ir-black",
    "isbl-editor-dark",
    "isbl-editor-light",
    "isotope",
    "kimber",
    "kimbie-dark",
    "kimbie.dark",
    "kimbie-light",
    "kimbie.light",
    "lightfair",
    "lioshi",
    "london-tube",
    "macintosh",
    "magula",
    "marrakesh",
    "materia",
    "material",
    "material-darker",
    "material-lighter",
    "material-palenight",
    "material-vivid",
    "mellow-purple",
    "mexico-light",
    "mocha",
    "mono-blue",
    "monokai",
    "monokai-sublime",
    "nebula",
    "night-owl",
    "nnfx",
    "nnfx-dark",
    "nnfx-light",
    "nord",
    "nova",
    "obsidian",
    "ocean",
    "oceanicnext",
    "onedark",
    "one-light",
    "outrun-dark",
    "panda-syntax-dark",
    "panda-syntax-light",
    "papercolor-dark",
    "papercolor-light",
    "paraiso",
    "paraiso-dark",
    "paraiso-light",
    "pasque",
    "phd",
    "pico",
    "pojoaque",
    "pop",
    "porple",
    "purebasic",
    "qtcreator-dark",
    "qtcreator_dark",
    "qtcreator-light",
    "qtcreator_light",
    "qualia",
    "railscasts",
    "rainbow",
    "rebecca",
    "ros-pine",
    "ros-pine-dawn",
    "ros-pine-moon",
    "routeros",
    "sagelight",
    "sandcastle",
    "school-book",
    "seti-ui",
    "shades-of-purple",
    "shapeshifter",
    "silk-dark",
    "silk-light",
    "snazzy",
    "solar-flare",
    "solar-flare-light",
    "solarized-dark",
    "solarized-light",
    "spacemacs",
    "srcery",
    "stackoverflow-dark",
    "stackoverflow-light",
    "summercamp",
    "summerfruit-dark",
    "summerfruit-light",
    "sunburst",
    "synth-midnight-terminal-dark",
    "synth-midnight-terminal-light",
    "tango",
    "tender",
    "tokyo-night-dark",
    "tokyo-night-light",
    "tomorrow",
    "tomorrow-night-blue",
    "tomorrow-night-bright",
    "tomorrow-night",
    "tomorrow-night-eighties",
    "twilight",
    "unikitty-dark",
    "unikitty-light",
    "vs2015",
    "vs",
    "vulcan",
    "windows-10",
    "windows-10-light",
    "windows-95",
    "windows-95-light",
    "windows-high-contrast",
    "windows-high-contrast-light",
    "windows-nt",
    "windows-nt-light",
    "woodland",
    "xcode",
    "xcode-dusk",
    "xt256",
    "zenburn"
];

const head: any = document.getElementsByTagName("head")[0];

const sourceCode: any = document.getElementById("source-code");
const renderedCode: any = document.getElementById("rendered-code");

const styleSelect: any = document.getElementById("styleSelector");

const nativeYes: any = document.getElementById("native-yes");
const nativeNo: any = document.getElementById("native-no");

const wordWrapYes: any = document.getElementById("yes");
const wordWrapNo: any = document.getElementById("no");

/************************************************************************************/
// Functions definition
/************************************************************************************/

// Function that retrieves current style
async function getPreferredStyle() {
    let getPreferredStyle = await browser.storage.local.get("vssStyle");
    let preferredStyle = getPreferredStyle["vssStyle"];

    if (!preferredStyle) {
        preferredStyle = "default";
    }

    return preferredStyle;
}

// Function that retrieves current native / VSS preference
async function getPreferredViewer(): Promise<boolean> {
    let getPreferredViewer = await browser.storage.local.get("vssNativeViewer");
    let preferredViewer = await getPreferredViewer["vssNativeViewer"];

    if (!preferredViewer) {
        preferredViewer = false;
    }

    return preferredViewer;
}

// Function that retrieves source / rendered code preference
async function getPreferredCode(): Promise<preferredCodeEnum> {
    let getPreferredCode = await browser.storage.local.get("vssCode");
    let preferredCode = await getPreferredCode["vssCode"];

    if (!preferredCode) {
        preferredCode = preferredCodeEnum.rendered;
    }

    return preferredCode;
}

// Function that retrieves current word wrap preference
async function getPreferredWordWrap(): Promise<boolean> {
    const getPreferredWordWrap: any = await browser.storage.local.get("vssWordWrap");
    let preferredWordWrap: boolean = getPreferredWordWrap["vssWordWrap"];

    if (!preferredWordWrap) {
        preferredWordWrap = false;
    }

    return preferredWordWrap;
}

// Function that sets the extension preferences
// and pre selections.
async function setOptions() {

    // Set style
    let preferredStyle = await getPreferredStyle();

    for (let i: number = 0; i < styles.length; i++) {
        let styleName: string = styles[i];
        let optionNode: any = document.createElement("OPTION");
        optionNode.value = styleName;
        let optionText: Text = document.createTextNode(styleName);
        optionNode.appendChild(optionText);
        if (preferredStyle === styleName) {
            optionNode.selected = "selected";
        }
        styleSelect.appendChild(optionNode);
    }

    // Set viewer
    let preferredViewer: boolean = await getPreferredViewer();

    if (preferredViewer) {
        if (nativeYes !== null) {
            nativeYes.setAttribute("checked", "true");
        }
    }

    if (!preferredViewer) {
        if (nativeNo !== null) {
            nativeNo.setAttribute("checked", "true");
        }
    }

    // Set code
    let preferredCode: preferredCodeEnum = await getPreferredCode();

    if (preferredCode === preferredCodeEnum.rendered) {
        if (renderedCode !== null) {
            renderedCode.setAttribute("checked", "true");
        }
    }

    if (preferredCode === preferredCodeEnum.source) {
        if (sourceCode !== null) {
            sourceCode.setAttribute("checked", "true");
        }
    }

    // Set word wrap
    let preferredWordWrap: boolean = await getPreferredWordWrap();

    if (preferredWordWrap) {
        if (wordWrapYes !== null) {
            wordWrapYes.setAttribute("checked", "true");
        }
    }

    if (!preferredWordWrap) {
        if (wordWrapNo !== null) {
            wordWrapNo.setAttribute("checked", "true");
        }
    }
}

// Function that writes the style value into
// local storage
async function changeStyle(event: any) {
    browser.storage.local.set({ "vssStyle": styleSelect.value });
    setStylesheet();
}

// Function that writes the viewer value 
// into local storage and hide / show
// the option fields accordingly
async function changeViewer(value: boolean) {
    browser.storage.local.set({ "vssNativeViewer": value });

    hideOrShowVSSOptions(value);
}

// Function that writes the code value 
// into local storage
async function changeCode(value: preferredCodeEnum) {
    browser.storage.local.set({ "vssCode": value });
}

// Function that sets word wrap value to yes
function setNativeoYes() {
    changeViewer(true);
}

// Function that sets word wrap value to no
function setNativeToNo() {
    changeViewer(false);
}

// Function that insert or remove CSS to hide
// or show the VSS options
function hideOrShowVSSOptions(value: boolean) {
    if (!value) {
        const nativeViewerStyleTag: HTMLElement | null = document.getElementById("vssViewer");
        if (nativeViewerStyleTag !== null) {
            nativeViewerStyleTag.remove();
        }
    }

    if (value) {
        let styleNode: any = document.createElement("STYLE");
        styleNode.id = "vssViewer";

        const style: string = ".if-vss { display: none; }";
        styleNode.innerText = style;

        const head: any = document.getElementsByTagName("head")[0];
        head.appendChild(styleNode);
    }
}

// Function that initialize the options shown
// according to local sotrage value
async function initialize() {
    let getPreferNativeViewer = await browser.storage.local.get("vssNativeViewer");
    let preferNativeViewer = await getPreferNativeViewer["vssNativeViewer"];

    hideOrShowVSSOptions(preferNativeViewer);
}

// Function that writes the word wrap value 
// into local storage
async function changeWordWrap(value: boolean) {
    browser.storage.local.set({ "vssWordWrap": value });
    setWordWrap();
}

// Function that sets word wrap value to yes
function setWordWrapToYes() {
    changeWordWrap(true);
}

// Function that sets word wrap value to no
function setWordWrapToNo() {
    changeWordWrap(false);
}

// Function that changes word wrap option selection
async function switchWordWrapSelection() {
    const getPreferredWordWrap: any = await browser.storage.local.get("vssWordWrap");
    let preferredWordWrap: boolean = getPreferredWordWrap["vssWordWrap"];

    if (preferredWordWrap) {
        wordWrapYes.setAttribute("checked", "true");
    } else {
        wordWrapNo.setAttribute("checked", "true");
    }
}

/************************************************************************************/
// Listeners definition
/************************************************************************************/

// Fill in options values and set the 
// previously selected options
window.addEventListener("load", setOptions);

// Listen to option changes
styleSelect.addEventListener("change", changeStyle);
nativeYes.addEventListener("change", setNativeoYes);
renderedCode.addEventListener("change", () => changeCode(preferredCodeEnum.rendered));
sourceCode.addEventListener("change", () => changeCode(preferredCodeEnum.source));
nativeNo.addEventListener("change", setNativeToNo);
wordWrapYes.addEventListener("change", setWordWrapToYes);
wordWrapNo.addEventListener("change", setWordWrapToNo);

// Listen to storage changes
browser.storage.onChanged.addListener(switchWordWrapSelection);

initialize();

// Start highlighting the code
//@ts-ignore
hljs.highlightAll();
