var fs = require('fs');
const path = require('node:path');

console.log("Bundling highlightjs-line-numbers.js...");

try {
    fs.rmSync(path.normalize('./addon/highlightjs-line-numbers.js/'), { recursive: true, force: true });
} catch {
    console.log("Directory 'addon/highlightjs-line-numbers.js' not found...");
}

fs.mkdirSync(path.normalize('./addon/highlightjs-line-numbers.js/'));
fs.copyFileSync(path.normalize('./node_modules/highlightjs-line-numbers.js/dist/highlightjs-line-numbers.min.js'), path.normalize('./addon/highlightjs-line-numbers.js/highlightjs-line-numbers.min.js'));
fs.copyFileSync(path.normalize('./node_modules/highlightjs-line-numbers.js/LICENSE'), path.normalize('./addon/highlightjs-line-numbers.js/LICENSE'));